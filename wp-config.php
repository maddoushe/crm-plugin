<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'crm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sfyrRN?rHb1-;kZKz*>R4ca/1$B&Zc<L?##t!|sL^Zr8{4;J5gK%6T)J#1Oyuo P' );
define( 'SECURE_AUTH_KEY',  '~Ve_aip1^zdaDJmz*pd1r8O<$nP|:,%O!6nlzHHJB]AO1q7vwJkg7Kk-TJy>f&i8' );
define( 'LOGGED_IN_KEY',    'q51}GWvg }ilJ;-l{`UIB>qiUI6D:f})n a7=p:yrUFy}F@M><k%$LofD`5.%ao{' );
define( 'NONCE_KEY',        'au1 lzsgyI@Ww?hSX5$R6a/YLd=#|?:-l=QvmJ)_Qw_k|K1DRnl3}|R]4_EsgIVv' );
define( 'AUTH_SALT',        '1cM@m]p5WNoV^PiQw8g(wbue)qtu+^ %XP]_[`tti(G4?Rq[n5hJ[}pM;i`<kV-A' );
define( 'SECURE_AUTH_SALT', 'uTA<*rCj4dG<d+)3l_!x&S4^(-hcZv_}OH4@(XCRwo-lEP1c8P%LAKDK:ch9M#U ' );
define( 'LOGGED_IN_SALT',   '*kff=1{HQNWAu(W)[~NG[$:=t,M&;6<Y^UKV}.U iS>OsR/Y,S&_Ng=`OnG:&6kJ' );
define( 'NONCE_SALT',       'Dd.bp,| ~Wi~^U+Nd/^%xo]su^D]$!8vu)1MbEZp-l60DHMJmJ-]4Yw_<2)__9wc' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'crm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

<?php get_header(); ?>
<section id="frontpage">
<div class="titre1"> 
    <h1>CRM : Le suivi</h1>
    <P>avec le nouveau CRM, l'organisation de vos clients devient un jeu d'enfants</P>
</div>   
<div class="titre2"> 
    <h3>Le CRM nous permetra de realiser les tache suivantes: </h3>
    <ul>
        <li>Permettre au client de créer des catégories à partir de l’application.</li>
        <li>Pouvoir archiver les relations client selon une configuration du plugin.</li>
        <li>filtrer les suivis par client.</li>
        <li>Pouvoir voir l’historique d’un suivi avec les clients</li>
    </ul>

</div> 
</section>
<?php get_footer(); ?> 
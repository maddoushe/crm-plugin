<?php
 /*
  Template Name: Demo
 */
 ?>
 
 <?php get_header(); ?>


     
<section id="apropos">
    <div class="container">
    <h1>La Demo</h1>
<br>

    <form>
      
      <label>
        <input type="text" placeholder="Non d'utilisateur"/>
      </label>

      <label>
        <input type="text" placeholder="Courriel"/>
      </label>

      <label for="ville">Choisir la ville:
        <select name="cars" id="cars">
            <option value="Québec">Québec</option>
            <option value="Montreal">Montreal</option>
            <option value="Gaspé">Gaspé</option>
            <option value="Gatineau">Gatineau</option>
        </select>
    </label>
    <label for="typemessage">Choisir le type de message:
        <select name="cars" id="cars">
            <option value="Entreprise">Entreprise</option>
            <option value="Client">Client</option>
            <option value="Gérant">Gérant</option>
            <option value="Employés">Employés</option>
        </select>
    </label>

      <label>
        <textarea placeholder="Message" rows="7" ></textarea>
      </label>

      <button class="red" type="button"><i></i>Envoyer</button>
    
    </form>

  </section>
  <section>
  <h2 >Le résultat</h2>

  <table class="darkTable">
    <thead>
        <tr>
            <th>Nom d'utilisateur</th>
            <th>Date</th>
            <th>Courriel</th>
            <th>Ville</th>
            <th>Type de message</th>
            <th>Message</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>mahdi</td>
            <td>17/10/2020</td>
            <td>mahdi@mahdi.com</td>
            <td>Montreal</td>
            <td>Client</td>
            <td>Lorem ipsum dolor sit amet.</td>
        </tr>
        <tr>
            <td>theo</td>
            <td>17/10/2020</td>
            <td>theo@theo.com</td>
            <td>Gaspé</td>
            <td>Employés</td>
            <td>Lorem ipsum dolor sit amet consectetur adipisicing.</td>
        </tr>
        <tr>
            <td>mahdi</td>
            <td>17/10/2020</td>
            <td>mahdi@mahdi.com</td>
            <td>Montreal</td>
            <td>Client</td>
            <td>Lorem ipsum dolor sit amet.</td>
        </tr>
        <tr>
            <td>theo</td>
            <td>17/10/2020</td>
            <td>theo@theo.com</td>
            <td>Gaspé</td>
            <td>Employés</td>
            <td>Lorem ipsum dolor sit amet consectetur adipisicing.</td>
        </tr>
    </tbody>
</table>
  </section>


<?php get_footer(); ?>